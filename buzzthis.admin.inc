<?php

/**
 * @file
 * Admin settings for the buzzthis module.
 */

/**
 * Admin settings form for the buzzthis module
 */
function buzzthis_admin_settings() {
  drupal_add_js(drupal_get_path('module', 'buzzthis') .'/buzzthis.js', 'module'); 
 
  $form = array();
  $form['buzzthis_follow'] = array(
    '#type' => 'fieldset',
    '#title' => 'Follow on Buzz',
    '#collapsible' => TRUE,
  );
  $form['buzzthis_follow']['buzzthis_profileid'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Buzz Profile ID'),
    '#default_value' => variable_get('buzzthis_profileid', ''),
    '#description' => t('Your Google Buzz Id or username (without @gmail.com). Don\'t know your profile URL? <a href="http://profiles.google.com/me">Click here</a>.'),
  );
  $form['buzzthis_button_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#default_value' => variable_get('buzzthis_button_style', 'count'),
    '#options' => array(
      'count' => t('Button + counter'),
      'button' => t('Button only'),
      'link' => t('Link'),
    ),
    '#id' => 'buzzthis-style',
    '#description' => t('This option controls the style of the buzzthis button.')
  );

  $form['buzzthis_post_count'] = array(
    '#type' => 'select',
    '#title' => t('Post Count'),
    '#default_value' => variable_get('buzzthis_post_count', 'normal'),
    '#options' => array(
      'normal' => t('On the top'),
      'small' => t('On the side'),
      ),
    '#id' => 'buzzthis-count',
    );

  $form['buzzthis_button_size'] = array(
    '#type' => 'select',
    '#title' => t('Size'),
    '#default' => variable_get('buzzthis_button_size', 'normal'),
    '#options' => array(
      'normal' => t('Normal'),
      'small' => t('Small'),
      ),
    '#id' => 'buzzthis-size',
    );

  $options = array(
    'ar' => 'Arabic',
    'bn' => 'Bengali',
    'bg' => 'Bulgarian',
    'ca' => 'Catalan',
    'zh' => 'Chinese',
    'zn_CN' => 'Chinese (China)',
    'zh_HK' => 'Chinese (Honk Kong)',
    'zh_TW' => 'Chinese (Taiwan)',
    'hr' => 'Croatian',
    'cs' => 'Czech',
    'da' => 'Danish',
    'nl' => 'Dutch',
    'en' => 'English',
    'en_IN' => 'English (India)',
    'en_IE' => 'English (Ireland)',
    'en_SG' => 'English (Singapore)',
    'en_ZA' => 'English (South Africa)',
    'en_GB' => 'English (United Kingdom)',
    'fil' => 'Filipino',
    'fi' => 'Finnish',
    'fr' => 'French',
    'de' => 'German',
    'de_CH' => 'German (Switzerland)',
    'el' => 'Greek',
    'gu' => 'Gujarati',
    'iw' => 'Hebrew',
    'hi' => 'Hindi',
    'hu' => 'Hungarian',
    'in' => 'Indonesian',
    'it' => 'Italian',
    'ja' => 'Japanese',
    'kn' => 'Kannada',
    'ko' => 'Korean',
    'lv' => 'Latvian',
    'ln' => 'Lingala',
    'lt' => 'Lithuanian',
    'ms' => 'Malay',
    'ml' => 'Malayalam',
    'mr' => 'Marathi',
    'no' => 'Norwegian',
    'or' => 'Oriya',
    'fa' => 'Persian',
    'pl' => 'Polish',
    'pt_BR' => 'Portuguese (Brazil)',
    'pt_PT' => 'Portuguese (Portugal)',
    'ro' => 'Romanian',
    'ru' => 'Russian',
    'sr' => 'Serbian',
    'sk' => 'Slovak',
    'sl' => 'Slovenian',
    'es' => 'Spanish',
    'sv' => 'Swedish',
    'gsw' => 'Swiss German',
    'ta' => 'Tamil',
    'te' => 'Telugu',
    'th' => 'Thai',
    'tr' => 'Turkish',
    'uk' => 'Ukrainian',
    'vi' => 'Vietnamese',
    );

  $form['buzzthis_button_language'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#options' => $options,
      '#default_value' => variable_get('buzzthis_button_language', 'en'),
    );

  $form['buzzthis_button_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#default_value' => variable_get('buzzthis_button_weight', -5),
    '#options' => drupal_map_assoc(range(-20, 20)),
    '#description' => t('Specifies the position of the buzzthis button.')
  );

  $form['buzzthis_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('buzzthis_node_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('The node types where the buzz button shall be displayed')
    );

  $form['buzzthis_location'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Location'),
    '#description' => t('Where to show the buzzthis button.'),
    '#options' => array(
      'content' => t('Full view'),
      'teasers' => t('Teasers'),
      ),  
    '#default_value' => variable_get('buzzthis_location', array()),
    ); 
  
  return system_settings_form($form);
}
