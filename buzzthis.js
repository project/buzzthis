
function buzzthis_style_status() {
  var style = $(this).val();
  if (style == 'count') {
    $('#buzzthis-count-wrapper').show();
    $('#buzzthis-size-wrapper').hide();
  }
  else if (style == 'button') {
    $('#buzzthis-count-wrapper').hide();
    $('#buzzthis-size-wrapper').show();
  }
  else {
    $('#buzzthis-count-wrapper').hide();
    $('#buzzthis-size-wrapper').hide();
  }

}

$(document).ready(function() {
  $('select#buzzthis-style').change(buzzthis_style_status).trigger('change');
});
